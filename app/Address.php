<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    public $timestamps = false;

    protected $table = 'address';

    protected $fillable = ['label', 'street', 'housenumber', 'postalcode', 'city', 'country'];

    protected $primaryKey = 'addressId';

    public static $rules = [
        'create' => [
            'label'       => 'required|string|max:100',
            'street'      => 'string|max:100',
            'housenumber' => 'string|max:10',
            'postalcode'  => 'string|max:6',
            'city'        => 'required|alpha|max:100',
            'country'     => 'required|alpha|max:100',
        ],
        'edit'   => [
            'label'       => 'string|max:100',
            'street'      => 'string|max:100',
            'housenumber' => 'string|max:10',
            'postalcode'  => 'string|max:6',
            'city'        => 'alpha|max:100',
            'country'     => 'alpha|max:100',
        ]
    ];

}
