<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/addresses', 'AddressController@index');

Route::post('/addresses', 'AddressController@store');

Route::get('/addresses/{addressid}', 'AddressController@show');

Route::put('/addresses/{addressid}', 'AddressController@update');

Route::patch('addresses/{addressid}', 'AddressController@update');

