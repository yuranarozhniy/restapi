<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Address;
use App\Http\Requests;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class AddressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $model = Address::all();

        return Response::json(array(
            'error' => false,
            'address' =>  $model->toArray()),
            200
        );
    }

    /**
     * Store the specified resource in storage.
     *
     * @param  object Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), Address::$rules['create']);

        if ($validator->fails()) {
            return Response::json(array(
                'error' => true,
                'message' => $validator->messages()),
                400
            );
        }

        if ($data = Address::create($request->all())) {
            return Response::json(array(
                'error' => false,
                'address' =>  $data->toArray()),
                201
            );
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $addressId
     * @param  object Request $request
     * @return Response
     */
    public function update($addressId, Request $request)
    {
        $validator = Validator::make($request->all(), Address::$rules['edit']);

        if ($validator->fails() || empty($request->all())) {
            return Response::json(array(
                'error' => true,
                'message' => empty($validator->messages()) ? $validator->messages() : 'Request data empty'),
                400
            );
        }

        $model = Address::find($addressId);

        if (is_null($model)) {
            return Response::json(array(
                'error' => true,
                'message' =>  "Address by addressId equal $addressId not found"),
                404
            );
        }

        if ($model->fill($request->all())->save()) {
            return Response::json(array(
                'error' => false,
                'address' =>  $model->toArray()),
                200
            );
        }

    }


    /**
     * Show resource by addressId.
     *
     * @param  int $addressId
     * @return Response
     */
    public function show($addressId)
    {
        $model = Address::find($addressId);

        if (is_null($model)) {
            return Response::json(array(
                'error' => true,
                'message' =>  "Address by addressId equal $addressId not found"),
                404
            );
        }

        return Response::json(array(
            'error' => false,
            'address' =>  $model->toArray()),
            200
        );
    }

}
