<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('address', function (Blueprint $table) {
            $table->increments('addressId')->comment = "The unique address ID.";
            $table->string('label', 100)->comment = "The name of the person or organisation to which the address belongs.";
            $table->string('street', 100)->comment = "The name of the street.";
            $table->string('housenumber', 10)->comment = "The house number (and any optional additions).";
            $table->string('postalcode', 6)->comment = "The postal code for the address.";
            $table->string('city', 100)->comment = "The city in which the address is located.";
            $table->string('country', 100)->comment = "The country in which the address is located.";
            $table->engine = 'InnoDB';
        });

        DB::table('address')->insert([
            [
                'label'       => 'First corp.',
                'street'      => 'Load str',
                'housenumber' => '20',
                'postalcode'  => '20412',
                'city'        => 'Lviv',
                'country'     => 'Ukraine',
            ],
            [
                'label'       => 'Second corp.',
                'street'      => 'Reload str',
                'housenumber' => '5',
                'postalcode'  => '204231',
                'city'        => 'Kiev',
                'country'     => 'Ukraine',
            ],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('address');
    }
}
